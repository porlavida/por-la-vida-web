import React from "react"
import { Link } from "gatsby"
import { graphql } from "gatsby"

import Layout from "../components/layout"
import Image from "../components/image"
import SEO from "../components/seo"

const ArticulosPage = ({ data }) => (
  <Layout>
    <SEO title="Artículos" />
    <h1>Artículos</h1>
    {data.allMarkdownRemark.edges.map(post => (
      <div key={post.node.id}>
        <h3>{post.node.frontmatter.titulo}</h3>
        <p>
          <span>
            <small>{post.node.frontmatter.fecha}</small>
          </span>
          <small> | </small>
          <span>
            <small>
              Por: <strong>{post.node.frontmatter.autor}</strong>
            </small>
          </span>
        </p>
        <p>
          <Link to={post.node.frontmatter.direccion}>Leer más...</Link>
        </p>
        <hr />
      </div>
    ))}
  </Layout>
)

export const pageQuery = graphql`
  query BlogIndexQuery {
    allMarkdownRemark {
      edges {
        node {
          id
          frontmatter {
            direccion
            titulo
            autor
            fecha
          }
        }
      }
    }
  }
`

export default ArticulosPage
