import React from "react"
import { Link } from "gatsby"

import Layout from "../components/layout"
import Image from "../components/image"
import SEO from "../components/seo"

const IndexPage = () => (
  <Layout>
    <SEO title="Inicio" />
    <h1>EDITORIAL</h1>
    <p>
      “Por la vida” Es un periódico elaborado por vecinos de la zona de Copilco,
      que busca ser un medio para visibilizar las voces, propuestas, debates,
      vivencias, acuerdos y desacuerdos. Expresiones culturales, artísticas de
      las distintas comunidades y grupos de afinidad que con-viven en la zona.
      Un espacio a disposición de los vecinos, para mostrar y promover los temas
      y prácticas de interés de estas comunidades.
    </p>
    <p>
      Tenemos la intención de promover el fortalecimiento de la comunicación y
      los lazos de convivencia, confianza, de dialogo entre los distintos grupos
      de las comunidades que habitan en la zona. Con el firme propósito de
      mejorar la vida comunitaria en Copilco.
    </p>
  </Layout>
)

export default IndexPage
