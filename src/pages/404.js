import React from "react"

import Layout from "../components/layout"
import SEO from "../components/seo"

const NotFoundPage = () => (
  <Layout>
    <SEO title="404: No encontrado" />
    <h1>NADA POR AQUÍ</h1>
    <p>El recurso solicitado no ha sido encontrado en la ubicación indicada, sirvase usar la caja de búsqueda adjunta para buscarlo:</p>
  </Layout>
)

export default NotFoundPage
