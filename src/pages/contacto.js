import React from "react"
import { Link } from "gatsby"

import Layout from "../components/layout"
import Image from "../components/image"
import SEO from "../components/seo"

const ContactoPage = () => (
  <Layout>
    <SEO title="Contacto" />
    <h1>Contáctanos</h1>
    <p>
      En Por la Vida les invitamos a que nos compartan sus proyectos y
      actividades en desarrollo en esta, nuestra comunidad de Copilco. Envíenos
      un correo e invítenos. ¡Hasta la próxima!
    </p>
    <div>
      <h3>NOS PUEDES CONTACTAR EN:</h3>
      <p>
        Facebook en la página: “No al helipuerto en Copilco (No al proyecto del
        Grupo Imagen Multimedia).
      </p>
      <p>Sábados en el CUC de 11 hrs. a 13 hrs.</p>
      <p>Odontología 35</p>
      <p>
        e-mail:{" "}
        <a href="mailto:vucuporlavida@gmail.com">vucuporlavida@gmail.com</a>
      </p>
    </div>
  </Layout>
)

export default ContactoPage
