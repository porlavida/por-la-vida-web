import React from "react"
import { Link } from "gatsby"

const Menu = () => (
  <nav className="menu">
    <ul>
      <li>
        <Link to="/">Inicio</Link>
      </li>
      <li>
        <Link to="/articulos">Artículos</Link>
      </li>
      <li>
        <Link to="/nosotros">¿Quiénes Somos?</Link>
      </li>
      <li>
        <Link to="/contacto">Contacto</Link>
      </li>
    </ul>
  </nav>
)

export default Menu
