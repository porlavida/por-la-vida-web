import React from "react"
import { Link } from "gatsby"
import { graphql } from "gatsby"

import Layout from "../components/layout"
import Image from "../components/image"
import SEO from "../components/seo"

export default function ArticuloTemplate({ data }) {
  const articulos = data.markdownRemark

  return (
    <Layout>
      <SEO title={articulos.frontmatter.titulo} />
      <Link to="/articulos">Regresar</Link>
      <hr />
      <h1>{articulos.frontmatter.titulo}</h1>
      <h4>
        Por: {articulos.frontmatter.autor} <span>|</span>{" "}
        <small>{articulos.frontmatter.fecha}</small>
      </h4>
      <div dangerouslySetInnerHTML={{ __html: articulos.html }} />
      <hr />
      <Link to="/articulos">Regresar</Link>
    </Layout>
  )
}

export const postQuery = graphql`
  query ArticuloPorPath($path: String!) {
    markdownRemark(frontmatter: { direccion: { eq: $path } }) {
      html
      frontmatter {
        direccion
        autor
        titulo
        fecha
      }
    }
  }
`
