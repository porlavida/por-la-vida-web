/**
 * Implement Gatsby's Node APIs in this file.
 *
 * See: https://www.gatsbyjs.org/docs/node-apis/
 */

// You can delete this file if you're not using it

const direccion = require("path")

exports.createPages = ({ actions, graphql }) => {
  const { createPage } = actions

  const plantillaEntrada = direccion.resolve('src/plantillas/articulo-entrada.js');

  return graphql(`
    {
      allMarkdownRemark {
        edges {
          node {
            html
            id
            frontmatter {
              direccion
              titulo
              autor
              fecha
            }
          }
        }
      }
    }
  `).then(res => {
    if(res.errors) {
      return Promise.reject(res.errors)
    }

    res.data.allMarkdownRemark.edges.forEach(({ node }) => {
      createPage({
        path: node.frontmatter.direccion,
        component: plantillaEntrada,
      })
    })
  })
}
